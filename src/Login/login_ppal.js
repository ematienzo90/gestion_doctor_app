import React, { Component } from "react";
import { Form, Input, Button } from 'antd';
import { UserOutlined, LockOutlined, EyeTwoTone, EyeInvisibleOutlined } from '@ant-design/icons';
import Logo from '../styles/icons/femaleDoc.png';
import { login} from '../actions/index';
import { bindActionCreators } from 'redux';
import {connect} from 'react-redux';

const onFinishFailed = errorInfo => {
  console.log('Failed:', errorInfo);
};

class Login extends Component {

  onSubmit(values) {
    console.log(values);
    this.props.loginPepe(values);
}

 
  render() {
    return (
      <Form style={{ width:"35%", margin: "auto", height: "80%", padding: "50px"}} name="Login" onFinish={values => this.onSubmit(values)} onFinishFailed={onFinishFailed}>
        <Form.Item>
          <img src={Logo} className="logo" />
        </Form.Item>
        <Form.Item
          name="usuario"
          rules={
            [
              { required: true, message: 'Introduzca el usuario' }
            ]
          }
        >
          <Input placeholder="Usuario" prefix={<UserOutlined className="site-form-item-icon" />} />
        </Form.Item>

        <Form.Item
          name="password"
          rules={
            [
              {
                required: true, message: 'Introduzca la contraseña'
              }
            ]
          }
        >
          <Input.Password 
            placeholder="Contraseña" 
            prefix={<LockOutlined className="site-form-item-icon" />}
            iconRender={visible => (visible ? <EyeTwoTone /> : <EyeInvisibleOutlined />)} />
        </Form.Item>

        <Form.Item >
            <Button type="primary" htmlType="submit" className="loginButton">
              Entrar
            </Button>
          
        </Form.Item>

      </Form>
    );
  }
}

function mapStateToProps(state) {
  return {
      
  };
}

function mapDispatchToProps(dispatch){
  return bindActionCreators({
      loginPepe: login
  }, dispatch);
}


export default connect(mapStateToProps, mapDispatchToProps)(Login);
