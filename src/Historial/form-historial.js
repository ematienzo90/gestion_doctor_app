import React, {Component}from 'react';
import { connect } from 'react-redux';
import { Link} from 'react-router-dom';
import { bindActionCreators } from 'redux';
import { Form, Input, InputNumber, Button, DatePicker } from 'antd';
import { crearHistoria, actualizarHistoria} from '../actions/index';
import moment from 'moment';

const { TextArea } = Input;

const layout = {
    labelCol: { span: 4 },
    wrapperCol: { span: 16 },
};

const validateMessages = {
    required: '${label} is required!',
    types: {
        anamnesis: '${label} is not validate anamnesis!',
        number: '${label} is not a validate number!',
    },
    number: {
        range: '${label} must be between ${min} and ${max}',
    },
};
class FormHistorial extends Component {

    constructor(props){
        super(props);
        this.state = {
            values: {
                id: (this.props.historia.id)? this.props.historia.id : null,
                af: (this.props.historia.id)? this.props.historia.af : null,
                alergias: (this.props.historia.id)? this.props.historia.alergias : null,
                masa_corporal: (this.props.historia.id)? this.props.historia.masaCorporal : null,
                fecha: (this.props.historia.id)? this.props.historia.fecha : null,
                anamnesis: (this.props.historia.id)? this.props.historia.anamnesis : null,
                exploracion: (this.props.historia.id)? this.props.historia.exploracion : null,
                complementaria: (this.props.historia.id)? this.props.historia.complementaria : null,
                diagnostico: (this.props.historia.id)? this.props.historia.diagnostico : null,
                tratamiento: (this.props.historia.id)? this.props.historia.tratamiento : null,
                paciente: this.props.paciente.dni
            },
            accion: (this.props.historia.id)? 'editar':'crear'
        };
    }

    componentDidMount(){
    }

    handleChange(event) {
        if (event){
            let value = null;
            let nombreCampo = null;
            if (event._isAMomentObject){
                value = event.format("DD/MM/YYYY");
                nombreCampo = 'fecha';
            } else {
                value = event.target.value;
                nombreCampo = event.target.name;
            }
            
            this.setState( prevState => {
                return { 
                values : {
                        ...prevState.values, [nombreCampo]: value
                    }
                }
            });
        }
    }

    onFinish(values) {
        if (this.state.accion === 'crear'){
            this.props.crearHistoria(this.state.values).then((respuesta) => {
                this.props.history.push("/ficha-paciente");
            });

        } else {
            this.props.actualizarHistoria(this.state.values).then((respuesta) => {
                this.props.history.push("/ficha-paciente");
            });

        }
    }

    render() {
        return (
            <Form {...layout} name="nest-messages" onFinish={values => this.onFinish(values)} validateMessages={validateMessages}>
            <Form.Item style={{marginLeft: 20}} label="Fecha" labelAlign="left">
                <DatePicker name= {'fecha'} defaultValue={this.state.values.fecha? moment(this.state.values.fecha, 'DD/MM/YYYY') : ''} format={'DD/MM/YYYY'} onChange={event => this.handleChange(event)} />
            </Form.Item>
            <Form.Item style={{marginLeft: 20}} label="A.F" labelAlign="left" rules={[{ required: true }]}>
                <Input name= {'af'} value={this.state.values.af} onChange={event => this.handleChange(event)} />
            </Form.Item>
            <Form.Item style={{marginLeft: 20}} label="Alergias" labelAlign="left" rules={[{ required: true }]}>
                <Input name= {'alergias'} value={this.state.values.alergias} onChange={event => this.handleChange(event)} />
            </Form.Item>
            <Form.Item style={{marginLeft: 20}} label="Motivo Consulta" labelAlign="left" rules={[{ required: true }]}>
                <TextArea rows={4} name= {'masa_corporal'} value={this.state.values.masa_corporal} onChange={event => this.handleChange(event)} />
            </Form.Item>
            <Form.Item style={{marginLeft: 20}} labelAlign="left" label="Complementaria">
                <TextArea rows={2} name= {'complementaria'} value={this.state.values.complementaria} onChange={event => this.handleChange(event)} />
            </Form.Item>
            <Form.Item style={{marginLeft: 20}} labelAlign="left" label="Diagnóstico">
                <TextArea rows={2}  name= {'diagnostico'} value={this.state.values.diagnostico} onChange={event => this.handleChange(event)} />
            </Form.Item>
            <Form.Item style={{marginLeft: 20}} labelAlign="left" label="Tratamiento">
                <TextArea rows={3} name= {'tratamiento'} value={this.state.values.tratamiento} onChange={event => this.handleChange(event)} />
            </Form.Item>
            <Form.Item style={{marginLeft: 20}} labelAlign="left" wrapperCol={{ ...layout.wrapperCol, offset: 8 }}>
                <Button type="primary" htmlType="submit">Guardar</Button>
            </Form.Item>
        </Form>
        );
    }
}

function mapStateToProps(state) {

    return {
        historia: state.activeHistoria,
        paciente: state.activePaciente
    };
  }
  
  function mapDispatchToProps(dispatch){
    return bindActionCreators({
        crearHistoria,
        actualizarHistoria
    }, dispatch);
  }
  
  export default connect(mapStateToProps, mapDispatchToProps)(FormHistorial);