import React, {Component}from 'react';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import { Button, Popconfirm, message  } from 'antd';
import { EditTwoTone, DeleteTwoTone, QuestionCircleOutlined} from '@ant-design/icons';
import { deleteHistoria,actualizarHistoria} from '../actions/index';
import { bindActionCreators } from 'redux';


class FichaHistoria extends Component {
    constructor(props){
        super(props);
        this.onConfirmDelete = this.onConfirmDelete.bind(this);

    }

    onClickUpdate(event){
        this.props.actualizarHistoria(this.props.activeHistoria.id);
    }

    onConfirmDelete(){
        this.props.deleteHistoria(this.props.activeHistoria.id);
    }

    render() {
        return (
        <div>
            <div style={{ marginBottom: '20px' }}>
            <Popconfirm
                title="¿Está seguro de eliminar esta ficha del historial?"
                onConfirm={this.onConfirmDelete}
                okText="Sí"
                cancelText="No"
                icon={<QuestionCircleOutlined style={{ color: 'red' }} />}
            >
                <Button style={{ float: 'right' }} type="primary" shape="round" icon={<DeleteTwoTone twoToneColor="#ffffff" />} size='large'>
                    Eliminar
                </Button>
            </Popconfirm>
                
                <div style={{float: 'right', width: '10px', visibility: 'hidden'}}>___</div>
                <Link  to="/form-historial" >
                    <Button style={{float: 'right'}} type="primary" shape="round" icon={<EditTwoTone twoToneColor="#ffffff" />} size='large'>
                        Editar
                    </Button>
                </Link>
            </div>

            <h2>Nº Historia: {this.props.activeHistoria.id}</h2>
            <h3>Fecha: {this.props.activeHistoria.fecha}</h3> 
            <h3 style={{ marginTop: '20px' }}>A.F: </h3><p>{this.props.activeHistoria.af}</p>
            <h3 style={{ marginTop: '20px' }}>Alergias: </h3><p>{this.props.activeHistoria.alergias}</p>
            <h3 style={{ marginTop: '20px' }}>Motivo Consulta: </h3><p>{this.props.activeHistoria.masaCorporal}</p>
            <h3 style={{ marginTop: '20px' }}>Complementaria: </h3><p>{this.props.activeHistoria.complementaria}</p>
            <h3 style={{ marginTop: '20px' }}>Diagnóstico </h3><p>{this.props.activeHistoria.diagnostico}</p>
            <h3 style={{ marginTop: '20px' }}>Tratamiento: </h3><p>{this.props.activeHistoria.tratamiento}</p>
           
        </div>
        );

    }
    
}

function mapStateToProps(state) {
    return {
        activeHistoria: state.activeHistoria,
    };
  }

  function mapDispatchToProps(dispatch){
    return bindActionCreators({
        deleteHistoria: deleteHistoria,
        actualizarHistoria
    }, dispatch);
  }

  
  export default connect(mapStateToProps, mapDispatchToProps)(FichaHistoria);
 