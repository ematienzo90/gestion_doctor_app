import React, { Component, useState } from "react";
import { Link } from 'react-router-dom';
import { List, Avatar, Button } from 'antd';
import { UserOutlined, UserAddOutlined } from '@ant-design/icons';
import { connect } from 'react-redux';
import SearchBar from '../containers/search_bar';
import { selectPaciente, fetchPacientes } from '../actions/index';
import { bindActionCreators } from 'redux';

class ListadoPacientes extends Component {
  constructor(props){
    super(props);
    this.state = {
      loading: true
    };

  }

  componentWillMount(){
  }
 
  render() {
    
    return (
      <div>
        <div style={{ float: 'right' }}>
          <Link to="/form-paciente" >
            <Button
              icon={<UserAddOutlined />}
              type="primary" 
              shape="circle" />
          </Link>
        </div>
        <h3 style={{textAlign: "center"}}>Listado de pacientes</h3>
        <SearchBar style={{textAlign: "center", width: "70%", marginLeft: "15%"}} />
        {(this.props.pacientes.length > 0)? (<List
            itemLayout="horizontal"
            dataSource={this.props.pacientes}
            style={{ padding: '20px' }}
            className="listadoPacientes"
            renderItem={item => (
                <Link to="/ficha-paciente" onClick={ () => this.props.selectPaciente(item)}>
                    <List.Item style={{backgroundColor: 'aliceblue' }}>
                        <List.Item.Meta
                            style={{margin: 15}}
                            avatar={<Avatar style={{ backgroundColor: '#87d068' }} icon={<UserOutlined />} />}
                            title={item.nombre + ' ' + item.apellidos}
                            description={<p>{item.profesion}</p>}
                        />
                        
                    </List.Item>
                </Link>
            )
          }
        />) : (<div></div>)}
        </div>
    );
  }
}

function mapStateToProps(state) {

  return {
    pacientes: state.pacientes
  };
}

function mapDispatchToProps(dispatch){
  return bindActionCreators({selectPaciente: selectPaciente, fetchPacientes: fetchPacientes }, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(ListadoPacientes);