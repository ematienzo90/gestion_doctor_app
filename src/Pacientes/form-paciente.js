import React, {Component}from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { Form, Input, InputNumber, Button, DatePicker, Select, Row, Col, Upload, Icon } from 'antd';
import { InboxOutlined , UploadOutlined} from '@ant-design/icons';
import { crearPaciente, actualizarPaciente } from '../actions/index';
import moment from 'moment';
import { Link, Redirect, useHistory } from 'react-router';


const { Dragger } = Upload;
const { Option } = Select;



const layout = {
    labelCol: { span: 4 },
    wrapperCol: { span: 16 },
};

const validateMessages = {
    required: '${label} is required!',
    types: {
        email: '${label} is not validate email!',
        number: '${label} is not a validate number!',
    },
    number: {
        range: '${label} must be between ${min} and ${max}',
    },
};



class FormPaciente extends Component {
    constructor(props){
        super(props);
        this.state = {
            values: {
                dni: this.props.paciente.dni,
                nombre: this.props.paciente.nombre,
                apellidos: this.props.paciente.apellidos,
                fecha_nacimiento: this.props.paciente.fechaNac,
                email: this.props.paciente.email,
                edad: this.props.paciente.edad,
                sexo: this.props.paciente.sexo,
                profesion: this.props.paciente.profesion,
                calle: this.props.paciente.calle,
                municipio: this.props.paciente.municipio,
                provincia: this.props.paciente.provincia,
                foto: this.props.paciente.foto
            },
            accion: (this.props.paciente.dni)? 'editar':'crear'            
        };

        this.onChangeFoto = this.onChangeFoto.bind(this);
        this.handleupload = this.handleupload.bind(this);


    }

    handleChange(event) {
        if (event){
            let value = null;
            let nombreCampo = null;
            if (event._isAMomentObject){
                value = event.format("DD/MM/YYYY");
                nombreCampo = 'fecha_nacimiento';
            } else {
                value = event.target.value;
                nombreCampo = event.target.name;
            }
            
            this.setState( prevState => {
                return { 
                values : {
                        ...prevState.values, [nombreCampo]: value
                    }
                }
            });
        }
    }

    handleChangeSexo(value, option){
        this.setState( prevState => {
            return { 
            values : {
                    ...prevState.values, ['sexo']: value
                }
            }
        });
    }

    handleChangeEdad(value){
        this.setState( prevState => {
            return { 
            values : {
                    ...prevState.values, ['edad']: value
                }
            }
        });
    }

    onFinish(values) {
        debugger;
        if (this.state.accion === 'crear'){
            this.props.crearPaciente(this.state.values).then((respuesta) => {
                this.props.history.push("/pacientes");
            });


        } else {
            this.props.actualizarPaciente(this.state.values).then((respuesta) => {
                this.props.history.push("/pacientes");
            });
        }
    }

    onChangeFoto(event){
        debugger;
        let files = event.target.files[0];
        let fileData = new Blob([files[0]]);
        debugger;
        // var promise = new Promise(getBuffer);
        // promise.then(function(data) {
        //     output.innerHTML = data.toString();
        //     console.log(data);
        // }).catch(function(err) {
        //     console.log('Error: ',err);
        // });
        this.state.values.foto=fileData;
        //const { status } = info.file;
        
        // if (status !== 'uploading') {
        // console.log(info.file, info.fileList);
        //     this.state.values.foto=info.fileList[0];
        // }
        // if (status === 'done') {
        // message.success(`Imagen ${info.file.name} cargada correctamente`);
        // } else if (status === 'error') {
        // message.error(`Error al cargar imagen ${info.file.name} `);
        // }
    }

    handleupload(info) {
        debugger;
        this.state.values.foto=info;
      }

    render() {

        return (
            <div>
          
                <Form {...layout} name="nest-messages" onFinish={values => this.onFinish(values)} validateMessages={validateMessages}>
                    <Form.Item style={{marginLeft: 20}} label="Foto" labelAlign="left" rules={[{ required: true }]}>
                        <Upload accept=".png,.jpg,.jpeg"
                            beforeUpload={file => {
                                debugger;
                                const reader = new FileReader();
                                var formData = new FormData();
                        
                                reader.onload = e => {
                                    console.log(e.target.result);
                                    this.state.values.foto=e.target.result;
                                };
                                reader.readAsDataURL(file);
                                
                                // Prevent upload
                                return false;
                            }}
                            action='https://www.mocky.io/v2/5cc8019d300000980a055e76'>
                            <Button>
                                <UploadOutlined />Click to Upload
                            </Button>
                        </Upload>
                        {/* <Dragger 
                            name='file'
                            multiple={false}
                            listType='picture'
                            action='https://www.mocky.io/v2/5cc8019d300000980a055e76' //ya hay una foto?¿
                            onChange={(info) => this.onChangeFoto(info)}
                            name= {'foto'}>
                            <p className="ant-upload-drag-icon">
                            <InboxOutlined />
                            </p>
                            <p className="ant-upload-text">Click aquí o arrastre una imagen</p>
                        </Dragger> */}
                        {/* <Input type="file" name="myfile" id="myfile" onChange={event => this.onChangeFoto(event)} /> */}

                    </Form.Item>
                    <Form.Item style={{marginLeft: 20}} label="DNI" labelAlign="left" rules={[{ required: true }]}>
                        <Input name= {'dni'} value={this.state.values.dni} onChange={event => this.handleChange(event)} />
                    </Form.Item>
                    <Form.Item style={{marginLeft: 20}} label="Nombre" labelAlign="left" rules={[{ required: true }]}>
                        <Input name= {'nombre'} value={this.state.values.nombre} onChange={event => this.handleChange(event)} />
                    </Form.Item>
                    <Form.Item style={{marginLeft: 20}} label="Apellidos" labelAlign="left" rules={[{ required: true }]}>
                        <Input name= {'apellidos'} value={this.state.values.apellidos} onChange={event => this.handleChange(event)} />
                    </Form.Item>
                    
                    <Form.Item style={{marginLeft: 20}} label="Email" labelAlign="left" rules={[{ type: 'email' }]}>
                        <Input name= {'email'} value={this.state.values.email} onChange={event => this.handleChange(event)} />
                    </Form.Item>
                    <Form.Item layout="inline" label="Fecha Nacimiento" labelAlign="left" style={{marginLeft: 20}} >
                        <Row gutter={24}>
                            <Col span={8}>
                                <Form.Item >
                                    <DatePicker name= {'fecha_nacimiento'} defaultValue={this.state.values.fecha_nacimiento? moment(this.state.values.fecha_nacimiento, 'DD/MM/YYYY') : ''} format={'DD/MM/YYYY'} onChange={event => this.handleChange(event)} />
                                </Form.Item>
                            </Col>
                            <Col span={8}>
                                <Form.Item label="Edad" labelAlign="left" rules={[{ type: 'number', min: 0, max: 140 }]}>
                                    <InputNumber name= {'edad'} value={this.state.values.edad} onChange={value => this.handleChangeEdad(value)} />
                                </Form.Item>
                            </Col>
                            <Col span={8}>
                                <Form.Item labelAlign="left" label="Sexo" rules={[{ required: true }]}>
                                    <Select  name= {'sexo'} value={this.state.values.sexo} onChange={(value,option) => this.handleChangeSexo(value,option)}>
                                        <Option value="Hombre">Hombre</Option>
                                        <Option value="Mujer">Mujer</Option>
                                    </Select>
                                </Form.Item>
                            </Col>
                        </Row>
                    </Form.Item>
                    <Form.Item style={{marginLeft: 20}} labelAlign="left" label="Profesión">
                        <Input name= {'profesion'} value={this.state.values.profesion} onChange={event => this.handleChange(event)} />
                    </Form.Item>
                    <Form.Item style={{marginLeft: 20}} labelAlign="left" label="Calle">
                        <Input name= {'calle'} value={this.state.values.calle} onChange={event => this.handleChange(event)} />
                    </Form.Item>
                    <Form.Item style={{marginLeft: 20}} labelAlign="left" label="Municipio">
                        <Input name= {'municipio'} value={this.state.values.municipio} onChange={event => this.handleChange(event)} />
                    </Form.Item>
                    <Form.Item style={{marginLeft: 20}} labelAlign="left" label="Provincia">
                        <Input name= {'provincia'} value={this.state.values.provincia} onChange={event => this.handleChange(event)} />
                    </Form.Item>
                    <Form.Item style={{marginLeft: 20}} labelAlign="left" wrapperCol={{ ...layout.wrapperCol, offset: 8 }}>
                        <Button type="primary" htmlType="submit">Guardar</Button>
                    </Form.Item>
                </Form>
            </div>
        );
    }

}

function mapStateToProps(state) {

    return {
        paciente: state.activePaciente,
        pacientes: state.pacientes
    };
  }
  
  function mapDispatchToProps(dispatch){
    return bindActionCreators({
        crearPaciente: crearPaciente,
        actualizarPaciente: actualizarPaciente
    }, dispatch);
  }

  FormPaciente.propTypes = {
    //form: PropTypes.object.isRequired
  };

  export default connect(mapStateToProps, mapDispatchToProps)(FormPaciente);