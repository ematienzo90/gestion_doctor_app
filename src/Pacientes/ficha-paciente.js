import React, {Component}from 'react';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import { List, Avatar,Button } from 'antd';
import { DiffTwoTone, EditTwoTone, DeleteTwoTone} from '@ant-design/icons';
import { fetchHistoriales, deletePaciente,selectHistoria } from '../actions/index';
import { bindActionCreators } from 'redux';


class FichaPaciente extends Component {
    constructor(props){
        super(props);
      }

    componentWillMount(){
        this.props.fetchHistoriales(this.props.paciente.dni);
    }

    onClickDelete(event){
        this.props.deletePaciente(this.props.paciente.dni);
    }

    render() {
        return (
            <div>
                <div style={{ marginBottom: '20px' }}>
                    <Link  to="/form-paciente" >
                        <Button style={{float: 'right'}} type="primary" shape="round" icon={<EditTwoTone twoToneColor="#ffffff" />} size='large'>
                            Editar
                        </Button>
                    </Link>
                    <div style={{float: 'right', width: '10px', visibility: 'hidden'}}>___</div>
                    <Button style={{ float: 'right' }} type="primary" onClick={event=>this.onClickDelete(event)} shape="round" icon={<DeleteTwoTone twoToneColor="#ffffff" />} size='large'>
                        Eliminar
                    </Button>
                </div>
                
                <div style={{ padding: '20px', float: 'left' }}>
                    {/* <Avatar shape="square" src={this.props.paciente.foto} size={200} /> */}
                    <img src={this.props.paciente.foto} style={{ width: '200px'}}  />
                </div>
                <div style={{ padding: '20px 40px 20px 20px', float: 'left' }}>
                    <h3>{this.props.paciente.nombre + ' ' + this.props.paciente.apellidos}</h3>
                    <span style={{marginBottom: '5px', display: 'inline-block'}}>
                        {this.props.paciente.edad} años ({this.props.paciente.fechaNac})
                    </span><br />
                    <span style={{marginBottom: '5px', display: 'inline-block'}}>
                        {this.props.paciente.domicilio}
                    </span><br />
                    <span style={{marginBottom: '5px', display: 'inline-block'}}>
                        {this.props.paciente.profesion}
                    </span><br />
                    <span style={{marginBottom: '5px', display: 'inline-block'}}>
                        {this.props.paciente.telefono}
                    </span><br />
                    <span style={{marginBottom: '5px', display: 'inline-block'}}>
                        {this.props.paciente.padres}
                    </span>
                </div>
                <div style={{clear: 'both', padding: '20px'}}>
                    <Link to="/form-historial" >
                        <Button
                        icon={<DiffTwoTone twoToneColor="#ffffff" />}
                        type="primary" 
                        shape="square">Crear Historia</Button>
                    </Link>
                    
                    <h3 style={{ marginTop: '5px', marginBottom: '5px'}}>HISTORIAL</h3>
                    <div>
                        {(this.props.historiales.length > 0)?
                        <List
                            itemLayout="horizontal"
                            dataSource={this.props.historiales}
                            renderItem={item => (
                                <Link to="/ficha-historia" onClick={ () => this.props.selectHistoria(item)}>
                                    <List.Item style={{backgroundColor: 'oldlace' }}>
                                        <List.Item.Meta
                                            style={{margin: 15}}
                                            title={item.fecha}
                                            description={<p>{item.diagnostico}</p>}
                                        />
                                        
                                    </List.Item>
                                </Link>
                                )
                            }
                        /> : <div></div>}
                    </div>

                </div>

            </div>
        );
    }
}

function mapStateToProps(state) {
    return {
        paciente: state.activePaciente,
        historiales: state.historiales
    };
  }

  function mapDispatchToProps(dispatch){
    return bindActionCreators({
        fetchHistoriales: fetchHistoriales, deletePaciente: deletePaciente, selectHistoria: selectHistoria
    }, dispatch);
  }

  
  export default connect(mapStateToProps, mapDispatchToProps)(FichaPaciente);
 