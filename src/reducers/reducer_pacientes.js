import {FETCH_PACIENTES,CREATE_PACIENTE, DELETE_PACIENTE, UPDATE_PACIENTE, REDIRECT_PACIENTE} from '../actions/index';

export default function(state=[], action){
    switch(action.type){
        case FETCH_PACIENTES:
            return action.payload;
        case CREATE_PACIENTE:
            return {...state, loadPacientes: true};
        case DELETE_PACIENTE:
            return {...state};
        case UPDATE_PACIENTE:
            return {...state, loadPacientes: true};
            
    }
    return state;
}