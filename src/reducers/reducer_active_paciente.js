import { SELECT_PACIENTE, DESELECT_PACIENTE } from '../actions/index';

export default function(state=[], action){
    switch(action.type){
        case SELECT_PACIENTE:
            return action.payload;
        case DESELECT_PACIENTE:
            return [];

    }
    return state;
}