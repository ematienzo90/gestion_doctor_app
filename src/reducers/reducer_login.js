import {LOGIN, LOGIN_SUCCESS, LOGIN_FAIL, LOGOUT} from '../actions/index';

export default function(state=[], action){
    switch(action.type){
        case LOGIN:
            return action.payload;

        case LOGIN_SUCCESS:
            return {...state, logged:true};

        case LOGIN_FAIL:
        case LOGOUT:
            return {...state, logged:false};

    }
    return state;
}