import { SELECT_HISTORIA, DESELECT_HISTORIA } from '../actions/index';

export default function(state=[], action){
    switch(action.type){
        case SELECT_HISTORIA:
            return action.payload;
        case DESELECT_HISTORIA:
            return [];

    }
    return state;
}