import { combineReducers } from 'redux';
import pacientesReducer from './reducer_pacientes';
import activePacienteReducer from './reducer_active_paciente';
import historialesReducer from './reducer_historiales';
import activeHistoriaReducer from './reducer_active_historia';
import loginReducer from './reducer_login';

const rootReducer = combineReducers({
  pacientes: pacientesReducer,
  activePaciente: activePacienteReducer,
  historiales: historialesReducer,
  activeHistoria: activeHistoriaReducer,
  login: loginReducer
});

export default rootReducer;