import {FETCH_HISTORIALES, DELETE_HISTORIA,CREATE_HISTORIA, UPDATE_HISTORIA} from '../actions/index';

export default function(state=[], action){
    switch(action.type){
        case FETCH_HISTORIALES:
            return action.payload;

        case CREATE_HISTORIA:
            return {...state};

        case DELETE_HISTORIA:
            return {...state};
            
        case UPDATE_HISTORIA:
            return {...state};

    }
    return state;
}