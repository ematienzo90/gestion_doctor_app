import React, {Component} from 'react';
import {connect} from 'react-redux';
import { Button, Input } from 'antd';
import {bindActionCreators} from 'redux';
import {fetchPacientes} from '../actions/index';

const { Search } = Input;

class SearchBar extends Component{
    constructor(props){
        super(props);
        this.state= { term:'' };
}

    onInputChange(event){
        this.setState({term: event.target.value})
    }

    onFormSubmit(value, event){
        event.preventDefault();
        this.setState({term: value})
        this.props.fetchPacientes(value);
        this.setState({term:''});
    }
    render(){
        return(
            <form style={this.props.style} 
                onSubmit={(value, event) => this.onFormSubmit(value, event)} 
                className="input-group"
            >
                <span className="input-group-btn">
                <Search placeholder="input search text" 
                    suffix={this.state.term} onSearch={(value, event) => this.onFormSubmit(value, event)} enterButton 
                />
                </span>
            </form>
        );
    }
}


function mapDispatchToProps(dispatch){
    return bindActionCreators({fetchPacientes},dispatch);
}

export default connect(null, mapDispatchToProps)(SearchBar);