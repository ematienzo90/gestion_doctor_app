import React from 'react';
import {render} from 'react-dom';
import './index.css';
import App from './App';
import { Provider } from "react-redux";

import { store } from "./_helpers";

render(
  <Provider store={store}>
    <App />
  </Provider>,
  //document.querySelector('.container')
  document.getElementById("root")
);
