import axios from 'axios';
import { notification } from 'antd';
import {history} from '../_helpers/history';
import { NavLink } from 'react-router-dom';
export const FETCH_PACIENTES='FETCH_PACIENTES';
export const SELECT_PACIENTE='SELECT_PACIENTE';
export const DESELECT_PACIENTE='DESELECT_PACIENTE';
export const FETCH_HISTORIALES='FETCH_HISTORIALES';
export const CREATE_PACIENTE='CREATE_PACIENTE';
export const UPDATE_PACIENTE='UPDATE_PACIENTE';
export const CREATE_HISTORIA='CREATE_HISTORIA';
export const DELETE_PACIENTE='DELETE_PACIENTE';
export const DELETE_HISTORIA='DELETE_HISTORIA';
export const SELECT_HISTORIA='SELECT_HISTORIA';
export const UPDATE_HISTORIA='UPDATE_HISTORIA';
export const DESELECT_HISTORIA='DESELECT_HISTORIA';
export const LOGIN='LOGIN';
export const LOGIN_SUCCESS='LOGIN_SUCCESS';
export const LOGIN_FAIL='LOGIN_FAIL';
export const LOGOUT='LOGOUT';
// export const REDIRECT_PACIENTE='REDIRECT_PACIENTE';

export function fetchPacientes(text){
    return (dispatch) => {
        return axios.get('http://localhost:8000/paciente?textBusq=' + text)
            .then(axiosResponse => {
                
                return axiosResponse.data
            })
            .then(dataFromAxios => {
                
                dispatch({
                    type: FETCH_PACIENTES,
                    payload: dataFromAxios
                });

                dispatch({
                    type: DESELECT_PACIENTE,
                    payload: dataFromAxios
                });

                dispatch({
                    type: DESELECT_HISTORIA,
                    payload: dataFromAxios
                })
            })
            .catch(error => {
                throw (error);
            });
    };
}

export function selectPaciente (paciente) {
    return{
        type: SELECT_PACIENTE,
        payload: paciente
    };
}

export function fetchHistoriales(paciente){
    return (dispatch) => {
        return axios.get(`http://localhost:8000/historia-paciente/${paciente}`)
        .then(axiosResponse => {

            return axiosResponse.data
        })
        .then(dataFromAxios => {

            dispatch({
                type: FETCH_HISTORIALES,
                payload: dataFromAxios
            })
        })
        .catch(error => {
            throw (error);
        });
    };

}

export function crearPaciente(datos){
    return (dispatch) => {
        return axios.post(`http://localhost:8000/paciente/crear`, datos)
        .then(axiosResponse => {

            return axiosResponse.data
        })
        .then(dataFromAxios => {
            notification[dataFromAxios.tipo_msg]({
                message: 'INFO',
                description: dataFromAxios.msg,
                duration: 2.5,
            });
            // dispatch({
            //     type: CREATE_PACIENTE,
            //     payload: dataFromAxios
            // });
            // .then(()=> {
            //     window.location.href="http://localhost:3000/pacientes";

            // });
        })
        .catch(error => {
            throw (error);
        });
    };
}

export function crearHistoria(datos){
    return (dispatch) => {
        return axios.post(`http://localhost:8000/historia/crear`, datos)
        .then(axiosResponse => {

            return axiosResponse.data
        })
        .then(dataFromAxios => {
            notification[dataFromAxios.tipo_msg]({
                message: 'INFO',
                description: dataFromAxios.msg,
                duration: 2.5,
            });
            dispatch({
                type: CREATE_HISTORIA,
                payload: dataFromAxios
            })
        })
        .catch(error => {
            throw (error);
        });
    };
}


export function deletePaciente(datos){
    
    return (dispatch) => {
        return axios.delete(`http://localhost:8000/paciente/delete`, {data: {
            dni:datos
        }})
        .then(axiosResponse => {

            return axiosResponse.data
        })
        .then(dataFromAxios => {
            notification[dataFromAxios.tipo_msg]({
                message: 'INFO',
                description: dataFromAxios.msg,
                duration: 2.5,
            });
            dispatch({
                type: DELETE_PACIENTE,
                payload: dataFromAxios
            })
        })
        .catch(error => {
            throw (error);
        });
    };

}


export function deleteHistoria(datos){
       
    return (dispatch) => {
        return axios.delete(`http://localhost:8000/historia/delete`, {data: {
            id:datos
        }})
        .then(axiosResponse => {

            return axiosResponse.data
        })
        .then(dataFromAxios => {
            notification[dataFromAxios.tipo_msg]({
                message: 'INFO',
                description: dataFromAxios.msg,
                duration: 2.5,
            });
            dispatch({
                type: DELETE_HISTORIA,
                payload: dataFromAxios
            })
        })
        .catch(error => {
            throw (error);
        });
    };
}


export function selectHistoria (historia) {
    return{
        type: SELECT_HISTORIA,
        payload: historia
    };
}

export function login(datos){
    return async (dispatch) => {
        return await axios.get('http://localhost:8000/appLogin?usuario='+datos.usuario+'&password='+datos.password)
        .then(axiosResponse => {

            return axiosResponse.data
        })
        .then(dataFromAxios => {
            notification[dataFromAxios.tipo_msg]({
                message: 'INFO',
                description: dataFromAxios.msg,
                duration: 1.5,
            });

            if (dataFromAxios.tipo_msg === "error"){
                dispatch({
                    type: LOGIN_FAIL,
                    payload: dataFromAxios
                })
            } else {
                dispatch({
                    type: LOGIN_SUCCESS,
                    payload: dataFromAxios
                })
                //history.push('/pacientes');
            }
             

        })
        .catch(error => {
            dispatch({
                type: LOGIN_FAIL,
                payload: []
            })
            throw (error);
        });
    };

}

export function logout(){
    return ({
        type: LOGOUT,
      });
    
}

export function actualizarPaciente(datos){
    return (dispatch) => {
        return axios.post(`http://localhost:8000/paciente/actualizar`, datos)
        .then(axiosResponse => {

            return axiosResponse.data
        })
        .then(dataFromAxios => {
            notification[dataFromAxios.tipo_msg]({
                message: 'INFO',
                description: dataFromAxios.msg,
                duration: 2.5,
            });
            //history.push("/paciente");
            dispatch({
                type: UPDATE_PACIENTE,
                payload: dataFromAxios
            })
        })
        .catch(error => {
            throw (error);
        });
    };
}

export function actualizarHistoria(datos){
    return (dispatch) => {
        return axios.post(`http://localhost:8000/historia/actualizar`, datos)
        .then(axiosResponse => {

            return axiosResponse.data
        })
        .then(dataFromAxios => {
            notification[dataFromAxios.tipo_msg]({
                message: 'INFO',
                description: dataFromAxios.msg,
                duration: 2.5,
            });
            //history.push("/paciente");
            //history.push(navigation.redirect);
             dispatch({
                type: UPDATE_HISTORIA,
                payload: dataFromAxios
            })
            // dispatch({
            //     type:REDIRECT_PACIENTE,
            //     payload: link
            // })
        })
        .catch(error => {
            throw (error);
        });
    };
}


