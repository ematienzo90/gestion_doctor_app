import React, {Component} from 'react';
import { Router, Route, Switch, Redirect, Link, BrowserRouter} from 'react-router-dom';
import Login from './Login/login_ppal';
import ListadoPacientes from './Pacientes/listado-pacientes';
import FichaPaciente from './Pacientes/ficha-paciente';
import FormPaciente from './Pacientes/form-paciente';
import FormHistorial from './Historial/form-historial';
import FichaHistoria from './Historial/ficha-historia';
import { history } from '../src/_helpers';
import 'antd/dist/antd.css';
import './App.css';
import { Layout, Menu, Button, ConfigProvider  } from 'antd';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { LogoutOutlined } from '@ant-design/icons';
import { logout } from '../src/actions/index';
import esEs from 'antd/es/locale/es_ES';

const { Header, Content, Footer } = Layout;

class App extends Component {
  constructor(props) {
    super(props);
  }

  render(){
    let {logged} = this.props.login;
    let displayMenu = 'none';
    if (logged === undefined){
      logged = false;
    }

    logged=true; //PROVISIONAL

    if (logged){
      displayMenu = 'block';
    }
    if (logged){
      return (
        <ConfigProvider locale={esEs}>
          <BrowserRouter history={history}>
            <Layout className="layout">
              <Header style={{ position: 'fixed', zIndex: 1, width: '100%', display: displayMenu }}>
                <div className="logo" />
                <Menu theme="dark" mode="horizontal" defaultSelectedKeys={['1']}>
                  <Menu.Item key="1"><Link to="/pacientes">Pacientes</Link></Menu.Item>
                  <Menu.Item key="3" style={{ float: 'right' }}><Button type="primary" onClick={this.props.logout}icon={<LogoutOutlined />} size={'large'} /></Menu.Item>
                </Menu>
              </Header>
              <Content className="site-layout" style={{ padding: '0 50px', marginTop: 64  }}>
                <div className="site-layout-background" style={{ padding: 24}}>
                  
                      <Switch>
                          <Route exact path="/pacientes" 
                            render={props => (
                              <ListadoPacientes {...props} />
                            )} />
                          <Route exact path="/ficha-paciente" 
                            render={props => (
                              <FichaPaciente {...props} />
                            )}  />
                          <Route exact path="/form-paciente" 
                            render={props => (
                              <FormPaciente {...props} />
                            )}  />
                          <Route exact path="/form-historial" 
                            render={props => (
                              <FormHistorial {...props} />
                            )}  />
                          <Route exact path="/ficha-historia"
                            render={props => (
                              <FichaHistoria {...props} />
                            )}  />
                          <Redirect to="/pacientes" />
                      </Switch>
                  
                </div>
              </Content>
              <Footer style={{ textAlign: 'center' }}>FCT ©2020 Emiliano Matienzo</Footer>
          </Layout>
        </BrowserRouter>
      </ConfigProvider>
      );
    } else {
      return (
        <ConfigProvider locale={esEs}>
          <Router history={history}>
            <Layout className="layout">
              <Content className="site-layout" style={{ padding: '0 50px', marginTop: 64  }}>
                <div className="site-layout-background" style={{ padding: 24}}>
                      <Switch>
                          <Route exact path="/login" component={Login} />
                          <Redirect to="/login" />
                      </Switch>
                  
                </div>
              </Content>
              <Footer style={{ textAlign: 'center' }}>FCT ©2020 Emiliano Matienzo</Footer>
          </Layout>
        </Router>
      </ConfigProvider>
      );
    }
  }
}

function mapStateToProps(state) {
  return {
      login: state.login
  };
}

function mapDispatchToProps(dispatch){
  return bindActionCreators({
      logout: logout
  }, dispatch);
}


export default connect(mapStateToProps, mapDispatchToProps)(App);